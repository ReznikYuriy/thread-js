import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});
export const getPostReaction = async (userId, postId) => {
  const reaction = await postReactionRepository.getPostReaction(userId, postId);
  // eslint-disable-next-line no-unneeded-ternary
  return (reaction) ? reaction : {};
};
export const setReaction = async (userId, { postId, isLike }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);
  // eslint-disable-next-line no-console
  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const updatePost = async (postId, data) => {
  await postRepository.updateById(postId, data);
};
