/* eslint-disable no-confusing-arrow */
import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  SET_EXPANDED_POST,
  DELETE_POST,
  EDIT_POST,
  UPDATE_POST,
  DELETE_COMMENT
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case EDIT_POST:
      return {
        ...state,
        posts: state.posts.map(post => post.id === action.postId ? { ...post, editing: true } : post)
      };
    case UPDATE_POST:
      return {
        ...state,
        posts: state.posts.map(post => post.id === action.post.postId ? { ...post,
          body: action.post.body,
          imageId: action.post.imageId,
          editing: !post.editing
        } : post)
      };
    case DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter(post => post.id !== action.postId)
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    case DELETE_COMMENT:
      return {
        ...state,
        expandedPost: { ...state.expandedPost,
          comments: state.expandedPost.comments.filter(comment => comment.id !== action.commentId),
          commentCount: Number(state.expandedPost.commentCount) - 1
        }
      };
    default:
      return state;
  }
};
