import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({ comment: { body, createdAt, user, id, postId }, currentUserId, deleteComment }) => {
  const IsCurrentUser = currentUserId === user.id;
  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{moment(createdAt).fromNow()}</CommentUI.Metadata>
        <CommentUI.Text>{body}</CommentUI.Text>
        {IsCurrentUser && (
          <CommentUI.Actions>
            <CommentUI.Action onClick={() => {}}>
              <Icon name="edit" />
              Edit
            </CommentUI.Action>
            <CommentUI.Action onClick={() => deleteComment(id, postId)}>
              <Icon name="eraser" />
              Del
            </CommentUI.Action>
          </CommentUI.Actions>
        )}
      </CommentUI.Content>
    </CommentUI>
  );
};
Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  currentUserId: PropTypes.string.isRequired,
  deleteComment: PropTypes.func.isRequired
};

export default Comment;
